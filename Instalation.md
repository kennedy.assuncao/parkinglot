Para executar a aplicação subir um container de Postgres com a seguintes definições:

postgres:
    image: postgres
    restart: always
    environment:
      POSTGRES_USER: "postgres"
      POSTGRES_PASSWORD: "postgres"
      PGDATA: "/var/lib/postgresql/data/pgdata"
    ports:
      - 5432:5432