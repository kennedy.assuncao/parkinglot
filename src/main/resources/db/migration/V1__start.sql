
create table vehicle (
    id bigint not null
        constraint vehicle_id
            primary key,
    plate           varchar(255),
    parking_time_id bigint
        constraint FK_parking_time_id
            references parking_time
);

create table parking_time (
    id bigint not null
        constraint parking_time_id
            primary key,
    entry_time  date,
    exited      boolean,
    output_time date,
    paid        boolean,
    stay        date,
    vehicle_id  bigint
        constraint FK_vehicle_id
            references vehicle
);