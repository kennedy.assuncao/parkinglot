package com.parking.parkinglot.parkinglot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parking.parkinglot.parkinglot.domain.ParkingTime;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 14 de out de 2020 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public interface ParkingTimeRepository extends JpaRepository < ParkingTime , Long > {
    
    

}
