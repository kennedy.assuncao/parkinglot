package com.parking.parkinglot.parkinglot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.parking.parkinglot.parkinglot.domain.Vehicle;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 14 de out de 2020 - @author Kennedy Assunção - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public interface VehicleRepository extends JpaRepository < Vehicle , Long > {
    
    @Query ( " select v from Vehicle v left join fetch v.parkingTime where v.plate =:plate " )
    List < Vehicle > findByHistory ( @Param ( "plate" ) String plate );

}
