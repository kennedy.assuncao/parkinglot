package com.parking.parkinglot.parkinglot.service;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parking.parkinglot.parkinglot.domain.ParkingTime;
import com.parking.parkinglot.parkinglot.domain.Vehicle;
import com.parking.parkinglot.parkinglot.exception.AlreadyPaidException;
import com.parking.parkinglot.parkinglot.exception.InvalidPlateFormatException;
import com.parking.parkinglot.parkinglot.exception.TransactionNotFoundException;
import com.parking.parkinglot.parkinglot.exception.TransactionPaymentException;
import com.parking.parkinglot.parkinglot.repository.ParkingTimeRepository;
import com.parking.parkinglot.parkinglot.repository.VehicleRepository;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 14 de out de 2020 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@Service
public class ParkingTimeService {

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    ParkingTimeRepository parkingTimeRepository;

    public boolean plateFormat ( String plate ) {
        Pattern pattern = Pattern.compile( "[A-Z]{3}-[0-9]{4}" );
        Matcher matcher = pattern.matcher( plate );
        return matcher.matches();
    }

    /**
     * TODO Descrição do Método
     * 
     * @param vehicle
     * @return
     */
    public List < Vehicle > findHistory ( String plate ) {
        List < Vehicle > vehicle = vehicleRepository.findByHistory( plate );
        return vehicle;
    }

    /**
     * TODO Descrição do Método
     * 
     * @param plate
     * @return
     * @throws InvalidPlateFormatException
     */
    public ParkingTime entry ( String plate, LocalDate date ) throws Exception {
        if ( plateFormat( plate ) ) {
            ParkingTime parking = new ParkingTime();
            parking.getVehicle().setPlate( plate );
            parking.setEntryTime( date );
            return parkingTimeRepository.save( parking );
        } else {
            throw new InvalidPlateFormatException( "Invalid Plate Format" );
        }
    }

    /**
     * TODO Descrição do Método
     * 
     * @param id
     * @throws Exception
     */
    public void makePayment ( Long id ) throws Exception {

        Optional < ParkingTime > transaction = parkingTimeRepository.findById( id );
        if ( transaction.isPresent() ) {
            var currentTransaction = transaction.get();
            if ( currentTransaction.getPaid() ) {
                throw new AlreadyPaidException( "Payment Already Made." );
            } else {
                currentTransaction.setPaid( Boolean.TRUE );
                parkingTimeRepository.save( currentTransaction );
            }
        }

    }

    /**
     * TODO Descrição do Método
     * 
     * @param id
     * @throws Exception
     */
    public void vehicleOutput ( Long id ) throws Exception {
        Optional < ParkingTime > transaction = parkingTimeRepository.findById( id );

        if ( transaction.isPresent() ) {
            var currentTransaction = transaction.get();
            if ( currentTransaction.getPaid() ) {
                currentTransaction.setPaid( Boolean.TRUE );
                currentTransaction.setOutputTime( LocalDate.now() );
                currentTransaction.setStay( Duration.between( currentTransaction.getEntryTime() ,
                        currentTransaction.getOutputTime() ) );
                parkingTimeRepository.save( currentTransaction );
            } else {
                throw new TransactionPaymentException( "Unpaid Transaction" );
            }
        } else
            throw new TransactionNotFoundException( "Transaction Not Found" );
    }

}
