package com.parking.parkinglot.parkinglot.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.parking.parkinglot.parkinglot.domain.ParkingTime;
import com.parking.parkinglot.parkinglot.domain.Vehicle;
import com.parking.parkinglot.parkinglot.service.ParkingTimeService;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 14 de out de 2020 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@RestController
@RequestMapping ( "/parking" )
public class ParkingLotController {

    @Autowired
    ParkingTimeService parkingTimeService;

    @GetMapping ( "/{plate}" )
    public ResponseEntity < ? > findVehicleHistory ( @PathVariable String plate ) {
        boolean plateFormat = parkingTimeService.plateFormat( plate );
        if ( plateFormat ) {
            List < Vehicle > history = parkingTimeService.findHistory( plate );
            return ResponseEntity.ok( history );
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity < Long > vehicleEntry ( @RequestBody Vehicle vehicle ) {
        try {
            ParkingTime entry = parkingTimeService.entry( vehicle.getPlate(), LocalDate.now() );
            return ResponseEntity.ok( entry.getId() );
        } catch ( Exception e ) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping ( "{id}/payment" )
    public < T > ResponseEntity < String > makePayment ( @PathVariable Long id ) {
        try {
            parkingTimeService.makePayment( id );
            return ResponseEntity.ok().build();
        } catch ( Exception e ) {
            return new ResponseEntity < String >( e.getMessage() , HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping ( "{id}/output" )
    public ResponseEntity < ? > vehicleOutput ( @PathVariable Long id ) {
        try {
            parkingTimeService.vehicleOutput( id );
            return ResponseEntity.ok().build();
        } catch ( Exception e ) {
            return new ResponseEntity < String >( e.getMessage() , HttpStatus.BAD_REQUEST );
        }
    }

}
