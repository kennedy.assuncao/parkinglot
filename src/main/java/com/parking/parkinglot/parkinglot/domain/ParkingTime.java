package com.parking.parkinglot.parkinglot.domain;

import java.time.Duration;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 13 de out de 2020 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@Entity
public class ParkingTime {

    @Id
    @GeneratedValue ( strategy = GenerationType.AUTO )
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Vehicle vehicle;
    private LocalDate entryTime;
    private LocalDate outputTime;
    private Duration stay;
    private Boolean paid;
    private Boolean exited;

    /**
     * Método de recuperação do campo id
     *
     * @return valor do campo id
     */
    public Long getId () {
        return id;
    }

    /**
     * Valor de id atribuído a id
     *
     * @param id
     *            Atributo da Classe
     */
    public void setId ( Long id ) {
        this.id = id;
    }

    /**
     * Método de recuperação do campo vehicle
     *
     * @return valor do campo vehicle
     */
    public Vehicle getVehicle () {
        return vehicle;
    }

    /**
     * Valor de vehicle atribuído a vehicle
     *
     * @param vehicle
     *            Atributo da Classe
     */
    public void setVehicle ( Vehicle vehicle ) {
        this.vehicle = vehicle;
    }

    /**
     * Método de recuperação do campo entryTime
     *
     * @return valor do campo entryTime
     */
    public LocalDate getEntryTime () {
        return entryTime;
    }

    /**
     * Valor de entryTime atribuído a entryTime
     *
     * @param entryTime
     *            Atributo da Classe
     */
    public void setEntryTime ( LocalDate entryTime ) {
        this.entryTime = entryTime;
    }

    /**
     * Método de recuperação do campo outputTime
     *
     * @return valor do campo outputTime
     */
    public LocalDate getOutputTime () {
        return outputTime;
    }

    /**
     * Valor de outputTime atribuído a outputTime
     *
     * @param outputTime
     *            Atributo da Classe
     */
    public void setOutputTime ( LocalDate outputTime ) {
        this.outputTime = outputTime;
    }

    /**
     * Método de recuperação do campo stay
     *
     * @return valor do campo stay
     */
    public Duration getStay () {
        return stay;
    }

    /**
     * Valor de stay atribuído a stay
     *
     * @param stay
     *            Atributo da Classe
     */
    public void setStay ( Duration stay ) {
        this.stay = stay;
    }

    /**
     * Método de recuperação do campo paid
     *
     * @return valor do campo paid
     */
    public Boolean getPaid () {
        return paid;
    }

    /**
     * Valor de paid atribuído a paid
     *
     * @param paid
     *            Atributo da Classe
     */
    public void setPaid ( Boolean paid ) {
        this.paid = paid;
    }

    /**
     * Método de recuperação do campo exited
     *
     * @return valor do campo exited
     */
    public Boolean getExited () {
        return exited;
    }

    /**
     * Valor de exited atribuído a exited
     *
     * @param exited
     *            Atributo da Classe
     */
    public void setExited ( Boolean exited ) {
        this.exited = exited;
    }

    /**
     * TODO Descrição do Método
     * 
     * @return
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode () {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( id == null ) ? 0 : id.hashCode() );
        return result;
    }

    /**
     * TODO Descrição do Método
     * 
     * @param obj
     * @return
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals ( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        ParkingTime other = ( ParkingTime ) obj;
        if ( id == null ) {
            if ( other.id != null )
                return false;
        } else if ( ! id.equals( other.id ) )
            return false;
        return true;
    }

    /**
     * TODO Descrição do Método
     * 
     * @return
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () {
        return "ParkingTime [id=" + id + ", vehicle=" + vehicle + ", entryTime=" + entryTime
                + ", outputTime=" + outputTime + ", stay=" + stay + ", paid=" + paid + ", exited="
                + exited + "]";
    }

}
