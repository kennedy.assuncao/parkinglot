package com.parking.parkinglot.parkinglot.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 13 de out de 2020 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@Entity
public class Vehicle {

    @Id
    @GeneratedValue ( strategy = GenerationType.AUTO )
    private Long id;
    private String plate;
    @OneToOne
    @JoinColumn ( name = "parking_time_id" )
    private ParkingTime parkingTime;

    /**
     * Método de recuperação do campo id
     *
     * @return valor do campo id
     */
    public Long getId () {
        return id;
    }

    /**
     * Valor de id atribuído a id
     *
     * @param id
     *            Atributo da Classe
     */
    public void setId ( Long id ) {
        this.id = id;
    }

    /**
     * Método de recuperação do campo plate
     *
     * @return valor do campo plate
     */
    public String getPlate () {
        return plate;
    }

    /**
     * Valor de plate atribuído a plate
     *
     * @param plate
     *            Atributo da Classe
     */
    public void setPlate ( String plate ) {
        this.plate = plate;
    }

    /**
     * Método de recuperação do campo parkingTime
     *
     * @return valor do campo parkingTime
     */
    public ParkingTime getParkingTime () {
        return parkingTime;
    }

    /**
     * Valor de parkingTime atribuído a parkingTime
     *
     * @param parkingTime
     *            Atributo da Classe
     */
    public void setParkingTime ( ParkingTime parkingTime ) {
        this.parkingTime = parkingTime;
    }

    /**
     * TODO Descrição do Método
     * 
     * @return
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode () {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( id == null ) ? 0 : id.hashCode() );
        return result;
    }

    /**
     * TODO Descrição do Método
     * 
     * @param obj
     * @return
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals ( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Vehicle other = ( Vehicle ) obj;
        if ( id == null ) {
            if ( other.id != null )
                return false;
        } else if ( ! id.equals( other.id ) )
            return false;
        return true;
    }

    /**
     * TODO Descrição do Método
     * 
     * @return
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () {
        return "Vehicle [id=" + id + ", plate=" + plate + ", parkingTime=" + parkingTime + "]";
    }
}
