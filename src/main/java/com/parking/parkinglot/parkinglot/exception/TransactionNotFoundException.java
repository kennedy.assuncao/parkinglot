package com.parking.parkinglot.parkinglot.exception;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 16 de out de 2020 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class TransactionNotFoundException extends Exception {

    private static final long serialVersionUID = 2168690247995784491L;

    public TransactionNotFoundException ( String msg ) {
        super( msg );
    }
}
