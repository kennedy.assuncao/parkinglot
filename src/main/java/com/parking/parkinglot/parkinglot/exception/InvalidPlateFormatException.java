package com.parking.parkinglot.parkinglot.exception;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 15 de out de 2020 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class InvalidPlateFormatException extends Exception {

    private static final long serialVersionUID = - 2212292158165777284L;

    public InvalidPlateFormatException ( String msg ) {
        super( msg );
    }

}
