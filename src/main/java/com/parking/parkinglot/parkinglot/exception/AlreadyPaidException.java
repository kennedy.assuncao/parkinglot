package com.parking.parkinglot.parkinglot.exception;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 16 de out de 2020 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class AlreadyPaidException extends Exception {

    private static final long serialVersionUID = - 4492868339545653922L;

    public AlreadyPaidException ( String msg ) {
        super( msg );
    }

}
