package com.parking.parkinglot.parkinglot.exception;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 16 de out de 2020 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class TransactionPaymentException extends Exception {

    private static final long serialVersionUID = 4694808969856784057L;

    public TransactionPaymentException ( String msg ) {
        super( msg );
    }

}
